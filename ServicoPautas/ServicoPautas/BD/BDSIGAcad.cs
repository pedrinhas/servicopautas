﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using ServicoPautas.Config;

namespace ServicoPautas.BD
{
    public static class BDSIGAcad
    {
        public static int utad_sp_pautas_estado_s(string codPauta)
        {
            try
            {
                using (var con = new OracleConnection(AppConfigKeys.ConnectionStrings.SIGAcad))
                using (var cmd = new OracleCommand("utad_sp_pautas_estado_s", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("pinCodPauta", OracleDbType.Varchar2, System.Data.ParameterDirection.Input).Value = codPauta;
                    cmd.Parameters.Add("poutEstado", OracleDbType.Int32, System.Data.ParameterDirection.Output);

                    con.Open();

                    var r = cmd.ExecuteNonQuery();

                    var res = cmd.Parameters["poutEstado"].Value.ToString();

                    int parsed = 0;
                    if (int.TryParse(res, out parsed))
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível obter o estado da pauta {0} no SIGAcad. res: {1}", codPauta, res));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
