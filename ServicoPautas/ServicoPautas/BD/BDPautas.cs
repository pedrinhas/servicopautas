﻿using ServicoPautas.Config;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoPautas.BD
{
    public static class BDPautas
    {
        #region Config

        public static string sp_Config_S(string key)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                {
                    var cmd = new SqlCommand("sp_Config_S", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@key", System.Data.SqlDbType.NVarChar).Value = key;

                    con.Open();

                    var res = cmd.ExecuteScalar().ToString();

                    return res;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion
        
        #region Datas

        public static int sp_Config_LastImport_U(DateTime date)
        {
            try
            {

                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                {
                    var cmd = new SqlCommand("sp_Config_LastImport_U", con);

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@date", System.Data.SqlDbType.DateTime).Value = date;

                    con.Open();

                    var res = cmd.ExecuteScalar().ToString();

                    int parsed = 0;
                    if (int.TryParse(res, out parsed))
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível atualizar a data da última importação ({0}). res: {1}", date.ToString(), res));
                    }
                }
            }
            catch (Exception ex)
            {
                //TRATAR
                throw ex;
            }
        }

        public static DateTime sp_Config_LastImport_S()
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Config_LastImport_S", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    con.Open();

                    var res = cmd.ExecuteScalar().ToString();

                    var parsed = DateTime.Now;
                    if (DateTime.TryParse(res, out parsed))
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível obter a data da última importação. res: {0}", res));
                    }
                }
            }
            catch (Exception ex)
            {
                //TRATAR
                throw ex;
            }
        }

        #endregion

        #region Pautas

        public class Pauta
        {
            public int ID { get; set; }
            public string CodPauta { get; set; }
            public DateTime WhenLacrado
            {
                get
                {
                    DateTime resDate = DateTime.Now;
                    DateTime resTime= DateTime.Now;

                    if (DateTime.TryParse(this.DataLacrado, out resDate) && DateTime.TryParse(this.HoraLacrado, out resTime))
                    {
                        return resDate.Add(new TimeSpan(resTime.Hour, resTime.Minute, resTime.Second));
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível converter a data/hora lacrado da pauta {0}. data: {1}, hora: {2}", this.CodPauta, this.DataLacrado, this.HoraLacrado));
                    }
                }
            }
            public string DataLacrado { get; set; }
            public string HoraLacrado { get; set; }
            public string Docente { get; set; }
            public int? EstadoSIGAcad { get; set; }
            public bool ImportedGesDoc { get; set; }
        }

        //sp_Pautas_Estado_S
        public static int sp_Pautas_Estado_S(string codPauta)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pautas_Estado_S", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codPauta", System.Data.SqlDbType.NVarChar).Value = codPauta;

                    con.Open();

                    var res = cmd.ExecuteScalar().ToString();
                    
                    int parsed = 0;
                    if(int.TryParse(res, out parsed))
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível obter o estado da pauta {0}. res: {1}", codPauta, res));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //sp_Pautas_Estado_U
        public static int sp_Pautas_Estado_U(string codPauta, int estado)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pautas_Estado_U", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codPauta", System.Data.SqlDbType.NVarChar).Value = codPauta;
                    cmd.Parameters.Add("@estadoSIGAcad", System.Data.SqlDbType.Int).Value = estado;

                    con.Open();

                    var res = cmd.ExecuteScalar().ToString();

                    int parsed = 0;
                    if (int.TryParse(res, out parsed))
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível atualizar o estado da pauta {0}. res: {1}, estado: {2}", codPauta, res, estado));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //sp_Pautas_I
        public static int sp_Pautas_I(Pauta pauta)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pautas_I", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codPauta", System.Data.SqlDbType.NVarChar).Value = pauta.CodPauta;
                    cmd.Parameters.Add("@dataLacrado", System.Data.SqlDbType.Date).Value = pauta.DataLacrado;
                    cmd.Parameters.Add("@horaLacrado", System.Data.SqlDbType.Time).Value = pauta.HoraLacrado;
                    cmd.Parameters.Add("@idDocente", System.Data.SqlDbType.NVarChar).Value = pauta.Docente;
                    cmd.Parameters.Add("@estadoSIGAcad", System.Data.SqlDbType.Int).Value = pauta.EstadoSIGAcad;
                    cmd.Parameters.Add("@importedGesDoc", System.Data.SqlDbType.Bit).Value = pauta.ImportedGesDoc;

                    con.Open();

                    var res = cmd.ExecuteScalar().ToString();

                    int parsed = 0;
                    if (int.TryParse(res, out parsed))
                    {
                        return parsed;
                    }
                    else
                    {
                        throw new FormatException(string.Format("Não foi possível inserir a pauta {0}. res: {1}", pauta.CodPauta, res));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //sp_Pautas_S
        public static Pauta sp_Pautas_S(string codPauta)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pauta_S", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codPauta", System.Data.SqlDbType.NVarChar).Value = codPauta;

                    con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var p = new Pauta();

                        //SEMPRE INT
                        p.ID = Convert.ToInt32(reader[0]);
                        p.CodPauta = reader[1].ToString();
                        p.DataLacrado = reader[2].ToString();
                        p.HoraLacrado = reader[3].ToString();
                        p.Docente = reader[4].ToString();
                        p.EstadoSIGAcad = Convert.ToInt32(reader[5]);
                        p.ImportedGesDoc = Convert.ToBoolean(reader[6]);

                        return p;
                    }

                    throw new ArgumentException(string.Format("Não foi possível obter a pauta {0}.", codPauta));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        //sp_Pautas_LS
        public static List<Pauta> sp_Pautas_LS()
        {
            try
            {
                var pautas = new List<Pauta>();

                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pautas_LS", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    con.Open();

                    var reader = cmd.ExecuteReader();

                    while(reader.Read())
                    {
                        var p = new Pauta();

                        //SEMPRE INT
                        p.ID = Convert.ToInt32(reader[0]);
                        p.CodPauta = reader[1].ToString();
                        p.DataLacrado = reader[2].ToString();
                        p.HoraLacrado = reader[3].ToString();
                        p.Docente = reader[4].ToString();
                        p.EstadoSIGAcad = Convert.ToInt32(reader[5]);
                        p.ImportedGesDoc = Convert.ToBoolean(reader[6]);

                        pautas.Add(p);
                    }
                    return pautas;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //sp_Pautas_ParaImportar_S
        public static Pauta sp_Pautas_ParaImportar_S(string codPauta)
        {
            try
            {
                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pautas_ParaImportar_S", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@codPauta", System.Data.SqlDbType.NVarChar).Value = codPauta;

                    con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var p = new Pauta();

                        //SEMPRE INT
                        p.ID = Convert.ToInt32(reader[0]);
                        p.CodPauta = reader[1].ToString();
                        p.DataLacrado = reader[2].ToString();
                        p.HoraLacrado = reader[3].ToString();
                        p.Docente = reader[4].ToString();
                        p.EstadoSIGAcad = Convert.ToInt32(reader[5]);
                        p.ImportedGesDoc = Convert.ToBoolean(reader[6]);

                        return p;
                    }

                    throw new ArgumentException(string.Format("Não foi possível obter a pauta {0}.", codPauta));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //sp_Pautas_ParaImportar_LS
        public static List<Pauta> sp_Pautas_ParaImportar_LS()
        {
            try
            {
                var pautas = new List<Pauta>();

                using (var con = new SqlConnection(AppConfigKeys.ConnectionStrings.Pautas))
                using (var cmd = new SqlCommand("sp_Pautas_ParaImportar_LS", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    con.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var p = new Pauta();

                        //SEMPRE INT
                        p.ID = Convert.ToInt32(reader[0]);
                        p.CodPauta = reader[1].ToString();
                        p.DataLacrado = reader[2].ToString();
                        p.HoraLacrado = reader[3].ToString();
                        p.Docente = reader[4].ToString();

                        pautas.Add(p);
                    }
                    return pautas;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
