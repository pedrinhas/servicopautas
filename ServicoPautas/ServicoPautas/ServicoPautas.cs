﻿using ServicoPautas.Config;
using ServicoPautas.Errors;
using ServicoPautas.Errors.Exceptions;
using ServicoPautas.Workers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ServicoPautas
{
    public partial class ServicoPautas : ServiceBase
    {
        public ServicoPautas()
        {
            InitializeComponent();
        }

        static void Main(string[] args)
        {
            ServicoPautas service = new ServicoPautas();

            if (Environment.UserInteractive)
            {
                service.StartDebug(args);
            }
            else
            {
                ServiceBase.Run(service);
            }
        }

        #region Métodos do ServiceBase

        protected override void OnStart(string[] args)
        {
            ServiceTimer.Initialize();
        }

        protected override void OnStop()
        {
            ServiceTimer.Dispose();
        }

        #endregion

        protected void StartDebug(string[] args)
        {
            PautasWorker.DoWork();
        }
    }
}
