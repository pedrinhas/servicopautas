﻿using System;
using System.Configuration;
using System.IO;
using System.Timers;
using System.Collections.Generic;
using ServicoPautas.Config;

namespace ServicoPautas.Workers
{
    public static class ServiceTimer
    {
        private static Timer mainTimer;
        private static Timer hourChecker;

        public static void Initialize()
        {
            hourChecker = new Timer(1000);
            hourChecker.Elapsed += OnHourCheckerEvent;
            hourChecker.Enabled = true;
            hourChecker.Start();
        }

        private static void InitializeMainTimer()
        {
            hourChecker.Dispose();

            mainTimer = new Timer(TimeSpan.FromDays(1).TotalMilliseconds);
            mainTimer.Elapsed += OnMainEvent;
            mainTimer.Enabled = true;
            mainTimer.Start();

            OnMainEvent(null, null);
        }

        private static void OnHourCheckerEvent(object onTimedEvent, ElapsedEventArgs e)
        {
            if (DateTime.Now.Hour == AppConfigKeys.Configuration.Timer.Hours && DateTime.Now.Minute == AppConfigKeys.Configuration.Timer.Minutes)
                InitializeMainTimer();

        }

        private static void OnMainEvent(object onTimedEvent, ElapsedEventArgs e)
        {
            //ir buscar pautas........


            PautasWorker.DoWork();
        }

        public static void Dispose()
        {
            mainTimer.Stop();
            mainTimer = null;
        }

    }
}
