﻿using ServicoPautas.Config;
using ServicoPautas.Errors;
using ServicoPautas.Errors.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ServicoPautas.BD;
using PDFToolsLib;

namespace ServicoPautas.Workers
{
    public static class PautasWorker
    {
        public static void DoWork()
        {
            try
            {
                //Fazer multithread
                //Ver o que fazer quando a pauta não existe.
                DateTime lastImport = BDPautas.sp_Config_LastImport_S();

                //string codPautaTeste = "7001NM2015A0669445"; //VALID
                //string codPautaTeste = "7001NM2015A0669425"; //INVALID

                int countPautasTeste = 50;

                //WS do SIDE a responder com data de lacrado, texto do ficheiro txt
                //var finalPautaASDASD = Pauta.Parse(GetPautaFromSIDE(codPautaTeste));


                //var res = GetPautasFromSIDE(DateTime.Now.AddYears(-1));
                var res = GetPautasFromSIDE(lastImport).Take(countPautasTeste).OrderBy(x => x.whenLacrado).ToList();

                foreach (var pauta in res.ToList())
                {
                    BDPautas.Pauta p = new BDPautas.Pauta();
                    p.CodPauta = pauta.numeropauta;
                    p.DataLacrado = pauta.dataLacrado;
                    p.HoraLacrado = pauta.horaLacrado;
                    p.Docente = pauta.idDocente;
                    p.EstadoSIGAcad = null;
                    p.ImportedGesDoc = false;

                    BDPautas.sp_Pautas_I(p);
                }

                foreach (var pauta in BDPautas.sp_Pautas_ParaImportar_LS())
                {
                    int estado = BDSIGAcad.utad_sp_pautas_estado_s(pauta.CodPauta);
                    if (estado == 10)
                    {
                        LogHelper.Log(string.Format("A gerar a pauta \"{0}\"", pauta.CodPauta), Severity.Info);

                        var finalPauta = Pauta.Parse(GetFicheiroPautaFromSIDE(pauta.CodPauta));

                        if (finalPauta == null || !finalPauta.Valid)
                        {
                            LogHelper.Log(string.Format("Não foi possível obter a pauta \"{0}\"", pauta.CodPauta), Severity.Error);
                        }
                        else
                        {
                            LogHelper.Log("Pauta obtida, a criar o PDF", Severity.Info);

                            string tempPdf = string.Format("{0}.pdf", finalPauta.Codigo);
                            File.WriteAllBytes(tempPdf, finalPauta.ToPDF());

                            LogHelper.Log(string.Format("PDF criado, a abrir..."), Severity.Success);

                            //System.Diagnostics.Process.Start(tempPdf);
                        }

                        BDPautas.sp_Pautas_Estado_U(pauta.CodPauta, estado);
                        BDPautas.sp_Config_LastImport_U(pauta.WhenLacrado);
                    }
                    else if(estado == 4)
                    {
                        BDPautas.sp_Pautas_Estado_U(pauta.CodPauta, estado);
                        BDPautas.sp_Config_LastImport_U(pauta.WhenLacrado);
                    }
                    else
                    {
                        //nada
                    }
                }

                //var last = res.Max(x => x.whenLacrado);
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, Severity.Error);
            }
        }

        private static List<SIDEPautaResponse> GetPautasFromSIDE(DateTime data)
        {
            var res = new List<SIDEPautaResponse>();
            string responseString = "";
            LogHelper.Log(string.Format("A obter listagem do SIDE a partir da data {0}", data.ToString("yyyy-MM-dd HH:mm:ss")), Severity.Info);

            try
            {
                string requestString = string.Format(AppConfigKeys.WebServices.SIDE.Endpoint + AppConfigKeys.WebServices.SIDE.Metodos.GetPautas, data.ToString("yyyy-MM-dd"), data.ToString("HH:mm:ss"));

                int statusCode = 500;

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestString);
                request.Method = "GET";

                request.Accept = "application/json;odata=verbose";

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    statusCode = (int)response.StatusCode;

                    MemoryStream ms = new MemoryStream();
                    response.GetResponseStream().CopyTo(ms);

                    responseString = Encoding.UTF7.GetString(ms.ToArray());
                }

                if (string.IsNullOrWhiteSpace(responseString)) throw new SIDEException(string.Format("Erro a obter as pautas a partir da data {0}.", data.ToString("yyyy-MM-dd HH:mm:ss")));

                System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                ser.MaxJsonLength = int.MaxValue;

                res = ser.Deserialize<List<SIDEPautaResponse>>(responseString);
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, Severity.Error);
                return null;
            }

            LogHelper.Log("Pautas obtidas com sucesso.", Severity.Info);

            return res;


        }

        private static string GetFicheiroPautaFromSIDE(string codPauta)
        {
            LogHelper.Log(string.Format("A pedir pauta \"{0}\" ao SIDE", codPauta), Severity.Info);

            string res = "";

            try
            {
                string requestString = string.Format(AppConfigKeys.WebServices.SIDE.Endpoint + AppConfigKeys.WebServices.SIDE.Metodos.GetFicheiroPauta, codPauta);

                int statusCode = 500;

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestString);
                request.Method = "GET";

                request.Accept = "application/json;odata=verbose";

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    statusCode = (int)response.StatusCode;

                    MemoryStream ms = new MemoryStream();
                    response.GetResponseStream().CopyTo(ms);

                    res = Encoding.UTF7.GetString(ms.ToArray());
                }

                if (string.IsNullOrWhiteSpace(res)) throw new SIDEException(string.Format("A pauta {0} não existe.", codPauta));
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, Severity.Error, string.Format("Erro a obter pauta {0} do SIDE.", codPauta));
                return "";
            }

            LogHelper.Log("Pauta obtida com sucesso.", Severity.Info);

            return res;
        }

        #region SIGAcad old
        /*
        private static List<Pauta> GetPautasFromSIGAcad(string ano)
        {
            var res = new List<Pauta>();
            LogHelper.Log(string.Format("A obter listagem do SIGAcad do ano {0}", ano), Severity.Info);

            try
            {
                using(var conn = new SqlConnection(AppConfigKeys.ConnectionStrings.IntermediaAcademSeg))
                using(var cmd = new SqlCommand())
                {
                    cmd.CommandText = "stp_Get_Pautas_LS";
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("@AnoIn", System.Data.SqlDbType.VarChar).Value = ano;

                    conn.Open();

                    var reader = cmd.ExecuteReader();

                    while(reader.Read())
                    {
                        Pauta p = new Pauta();

                        p.Codigo = reader["codPauta"].ToString();
                        p.AnoLetivo = reader["anoLectivo"].ToString();
                        p.DepartamentoCod = reader["codDepartamento"].ToString();
                        p.DepartamentoNome = reader["nomeDepartamento"].ToString();
                        p.EscolaCodNacional = reader["codNacionalEscola"].ToString();
                        p.EscolaCod = reader["codEscola"].ToString();
                        p.EscolaNome = reader["nomeEscola"].ToString();
                        p.ResponsavelPreenchimento = reader["responsavelPreenchimentoPauta"].ToString();
                        p.ResponsavelPreenchimentoNome= reader["responsavelNomePreenchimentoPauta"].ToString();

                        //GUARDAR DATA, AINDA NÃO SAI NO PROCEDURE

                        res.Add(p);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, Severity.Error, res.Count == 0 ? "" : string.Format("Erro a obter a pauta a seguir à {0}", res.Last().Codigo));
                throw ex;
            }

            LogHelper.Log(string.Format("Listagem obtida ({0} pauta{1})", res.Count, res.Count == 1 ? "" : "s"), Severity.Info);

            return res;
        }
        */
        #endregion
    }
}
