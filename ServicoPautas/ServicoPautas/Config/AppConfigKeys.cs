﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoPautas.Config
{
    public static class AppConfigKeys
    {
        private static string getValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        public static class WebServices
        {
            public static class SIDE
            {
                public static string Endpoint { get { return getValue("WebServices.SIDE.Endpoint").TrimEnd('/') + '/'; } }
                public static class Metodos
                {
                    public static string GetFicheiroPauta { get { return getValue("WebServices.SIDE.Metodos.GetFicheiroPauta"); } }
                    public static string GetPautas { get { return getValue("WebServices.SIDE.Metodos.GetPautas"); } }
                }
            }
            public static class GesDoc
            {
                public static string Endpoint { get { return getValue("WebServices.GesDoc.Endpoint").TrimEnd('/') + '/'; } }
                public static class Metodos
                {
                }
            }
            public static class IAcad
            {
                public static string Endpoint { get { return getValue("WebServices.IAcad.Endpoint").TrimEnd('/') + '/'; } }
                public static class Metodos
                {
                    public static string GetPauta { get { return getValue("WebServices.IAcad.Metodos.GetPauta"); } }
                }
            }
        }

        public static class Configuration
        {
            public static class Timer
            {
                public static int Hours
                {
                    get
                    {
                        int res = 20;

                        int.TryParse(getValue("Configuration.Timer.Hours"), out res);

                        return res;
                    }
                }
                public static int Minutes
                {
                    get
                    {
                        int res = 20;

                        int.TryParse(getValue("Configuration.Timer.Minutes"), out res);

                        return res;
                    }
                }
            }
        }

        public static class ConnectionStrings
        {
            private static string getConnString(string name) { return ConfigurationManager.ConnectionStrings[name].ConnectionString; }
            public static string SIGAcad { get { return getConnString("cStringSIGAcad"); } }
            //public static string SIGAcadTNS { get { return getConnString("cStringSIGAcadTNS"); } }
            public static string Maestro { get { return getConnString("cStringMaestro"); } }
            public static string Intermedia { get { return getConnString("cStringIntermedia"); } }
            public static string Pautas { get { return getConnString("cStringPautas"); } }
            public static string IntermediaAcademSeg { get { return getConnString("cStringIntermediaAcademSeg"); } }
        }
    }
}
/*
 
            public class WS
            {
                public static string Url { get { return @"http://localhost:1537/Acad/"; } }
                public static string GetPautaMethodSignature { get { return @"GetPauta?numPauta={0}"; } }
            }
 
 
 */