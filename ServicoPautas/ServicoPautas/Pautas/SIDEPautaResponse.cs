﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoPautas
{
    [Serializable]
    public class SIDEPautaResponse
    {
        public string idDocente { get; set; }
        public string numeropauta { get; set; }
        public string dataLacrado { get; set; }
        public string horaLacrado { get; set; }
        public DateTime whenLacrado { get { return DateTime.Parse(string.Format("{0} {1}", dataLacrado, horaLacrado)); } }
    }
}
