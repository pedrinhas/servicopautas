﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using Microsoft.Win32;
using PDFToolsLib;
//using PDFToolsLib;
using ServicoPautas.Config;
using ServicoPautas.Errors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ServicoPautas
{
    [Serializable]
    public class Pauta
    {
        private bool parsed = false;

        protected float caret = 692f;

        public string Codigo { get; set; }
        public string CodDisciplina { get; set; }
        public string NomeDisciplina { get; set; }
        public string Disciplina { get { return string.Format("{0} - {1}", CodDisciplina, NomeDisciplina); } }
        public string CodCurso { get; set; }
        public string NomeCurso { get; set; }
        public string Curso { get { return string.Format("{0} - {1}", CodCurso, NomeCurso); } }
        public string EpocaExame { get; set; }
        public string TipoExame { get; set; }
        public string ECTS { get; set; }
        public string DataExame { get; set; }
        public string AnoLetivo { get; set; }
        public List<LinhaPauta> TabelaNotas { get; private set; }
        public Dictionary<string, string> LegendaNotas { get; private set; }


        public string EscolaNome { get; set; }
        public string EscolaCod { get; set; }
        public string DepartamentoNome { get; set; }
        public string DepartamentoCod { get; set; }
        public string EscolaCodNacional { get; set; }
        public string ResponsavelPreenchimento { get; set; }
        public string ResponsavelPreenchimentoNome { get; set; }

        public bool Valid { get; private set; }

        public Pauta()
        {
            TabelaNotas = new List<LinhaPauta>();
            LegendaNotas = new Dictionary<string, string>();

            LegendaNotas.Add("0", "Zero");
            LegendaNotas.Add("1", "Um");
            LegendaNotas.Add("2", "Dois");
            LegendaNotas.Add("3", "Três");
            LegendaNotas.Add("4", "Quatro");
            LegendaNotas.Add("5", "Cinco");
            LegendaNotas.Add("6", "Seis");
            LegendaNotas.Add("7", "Sete");
            LegendaNotas.Add("8", "Oito");
            LegendaNotas.Add("9", "Nove");
            LegendaNotas.Add("10", "Dez");
            LegendaNotas.Add("11", "Onze");
            LegendaNotas.Add("12", "Doze");
            LegendaNotas.Add("13", "Treze");
            LegendaNotas.Add("14", "Catorze");
            LegendaNotas.Add("15", "Quinze");
            LegendaNotas.Add("16", "Dezasseis");
            LegendaNotas.Add("17", "Dezassete");
            LegendaNotas.Add("18", "Dezoito");
            LegendaNotas.Add("19", "Dezanove");
            LegendaNotas.Add("20", "Vinte");

            Valid = false;
        }
        public Pauta(bool _parsed)
            : this()
        {
            parsed = _parsed;
        }

        public static Pauta Parse(string s)
        {
            Pauta res = null;
            try
            {
                if (string.IsNullOrWhiteSpace(s)) return null;
                res = new Pauta(true);

                var lines = s.Replace("\r", "").Split('\n').ToList();

                //TABELA DE LEGENDA
                foreach (var line in lines)
                {
                    var split = line.Split('\t');
                    string beggining = split.First();

                    switch (beggining)
                    {
                        case "HDIC01":
                            //PARSE HEADER TABELA LEGENDA
                            break;
                        case "BDIC01":
                            //PARSE LINHA LEGENDA

                            string key = split[1];
                            string value = split[2];
                            //Reprovou por Nota Mínima
                            var splitVal = value.Split(' ');
                            if (splitVal.Length > 2)
                            {
                                string newValue = "";
                                int processed = 0;

                                foreach (var v in splitVal)
                                {
                                    processed++;

                                    if (char.IsLower(v.First())) continue;

                                    if (processed == splitVal.Length && (newValue + v).Length < 12)
                                    {
                                        newValue += v;
                                    }
                                    else
                                    {
                                        if (v.Length > 5)
                                        {
                                            newValue += string.Format("{0}. ", v.Substring(0, 3));
                                        }
                                        else
                                        {
                                            newValue += string.Format("{0} ", v);
                                        }
                                    }
                                }
                                newValue = newValue.Trim();
                                res.LegendaNotas.Add(key, newValue);
                            }
                            else
                            {
                                res.LegendaNotas.Add(key, value);
                            }

                            break;
                        case "FDIC01":
                            //PARSE FOOTER TABELA LEGENDA
                            break;
                        default:
                            continue;
                    }
                }

                //RESTO
                foreach (var line in lines)
                {
                    var split = line.Split('\t');
                    string beggining = split.First();

                    switch (beggining)
                    {
                        case "HDADOS":
                            //PARSE HEADER

                            res.Codigo = split[9];
                            res.CodDisciplina = split[2];
                            res.NomeDisciplina = split[3];
                            res.EpocaExame = string.Format("{0}º SEMESTRE", split[6]);
                            res.TipoExame = split[5];
                            res.DataExame = DateTime.Parse(split[12]).ToString("dd-MM-yyyy");
                            res.AnoLetivo = string.Format("{0} / {1}", split[4], Convert.ToInt32(split[4]) + 1);
                            break;
                        case "BDADOS":
                            //PARSE LINHA
                            var linha = new LinhaPauta();

                            linha.Reg = split[4];
                            linha.Numero = split[1];
                            linha.Nome = split[2];
                            linha.Nota = split[7];
                            linha.Extenso = res.LegendaNotas[linha.Nota];
                            linha.Rubrica = linha.Nota.Equals("77") ? "----------" : "";
                            linha.Observacoes = Convert.ToInt32(linha.Nota) > 20 ? linha.Extenso : Convert.ToInt32(linha.Nota) >= 10 ? "Aprovado" : "Reprovado";

                            if (string.IsNullOrWhiteSpace(res.CodCurso))
                            {
                                res.CodCurso = split[3];
                                res.NomeCurso = "NOME PLACEHOLDER - IR BUSCAR AO WS";
                            }

                            res.TabelaNotas.Add(linha);

                            break;
                        case "FDADOS":
                        default:
                            continue;
                    }
                }

                res.TabelaNotas = res.TabelaNotas.OrderBy(x => x.Nome).ToList();

                if (!res.ReadDataFromIAcad())
                {
                    throw new WebException(string.Format("Não foi possível aceder ao serviço \"{0}\"", AppConfigKeys.WebServices.IAcad.Endpoint));
                }

            }
            catch (WebException ex)
            {
                LogHelper.Log(ex, Severity.Error, string.Format("Erro ao obter informação da pauta pelo web service. Num pauta: {0}", res.Codigo));
            }
            catch (Exception ex)
            {

            }

            res.Valid = true;
            return res;
        }
        public static Pauta Parse(FileStream fs)
        {
            using (StreamReader reader = new StreamReader(fs))
            {
                var res = Parse(reader.ReadToEnd());

                fs.Close();
                fs.Dispose();

                return res;
            }
        }

        public byte[] ToPDF()
        {
            try
            {
                var prePDF = CreatePDFInternal();

                int pageCount = 0;

                using (PdfReader reader = new PdfReader(prePDF))
                {
                    pageCount = reader.NumberOfPages;
                }

                return CreatePDFInternal(pageCount);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private byte[] CreatePDFInternal(int pageCount = -1)
        {
            if (parsed)
            {
                bool simulating = pageCount == -1;
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                //Margens da primeira página, muda na segunda página dentro do HeaderFooterBuilderPautas
                int u = 200 + Settings.PDF.SpaceBeforePauta, d = 65, l = 45, r = 45;

                string colorProfilePath = @"..\..\Pautas\resources\sRGB Color Space Profile.icm";
                MemoryStream stream = new MemoryStream();

                using (FileStream fsColor = new FileStream(colorProfilePath, FileMode.Open, FileAccess.Read))
                using (Document doc = new Document(PageSize.A4))
                using (PdfAWriter writer = PdfAWriter.GetInstance(doc, stream, PdfAConformanceLevel.PDF_A_3B))
                {
                    #region Tipos de letra

                    var headerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 28, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                    var defaultBorder = Settings.PDF.NoBorder;

                    var titleFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                    var footerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                    var footerBoldFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                    var footerLinesFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 4, iTextSharp.text.Font.BOLD, BaseColor.GRAY);

                    var bodyFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                    var bodyFontBold = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                    var bodyFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                    var smallerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                    var smallerFontBold = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                    var smallerFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                    var smallFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                    #endregion

                    #region Setup

                    var newPageEvent = new HeaderFooterBuilderPautas(this, doc.PageSize, pageCount);

                    writer.PageEvent = newPageEvent;
                    if (!doc.IsOpen()) doc.Open();

                    doc.AddTitle(string.Format("Pauta cod. {0}", this.Codigo));
                    doc.AddHeader("Tipo de documento", "Pauta");

                    float top = doc.PageSize.Top - newPageEvent.GetFrontHeaderSize() - doc.TopMargin - Settings.PDF.SpaceBeforePauta;

                    doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.PageSize.Top - top, doc.BottomMargin);


                    ICC_Profile colorProfile = ICC_Profile.GetInstance(fsColor.ToByteArray());

                    writer.SetOutputIntents("Color Space", "a", "www.color.org", "sRGB IEC61966-2.1", colorProfile);

                    #endregion

                    doc.NewPage();

                    #region Body - Pauta

                    #region Cabeçalho da tabela

                    var pautaTable = new PdfPTable(7)
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        SpacingBefore = Settings.PDF.SpaceBeforePauta,
                        TotalWidth = doc.Right - doc.Left,
                    };
                    pautaTable.DefaultCell.Border = Settings.PDF.AllBorders;
                    pautaTable.SetWidthPercentage(new float[] { 9f, 5f, 50f, 5f, 12f, 7f, 12f }, doc.PageSize);
                    pautaTable.WidthPercentage = 100f;
                    var numeroHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var numeroHeaderParagraph = new Paragraph("Número", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    numeroHeaderCell.AddElement(numeroHeaderParagraph);

                    var regHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var regHeaderParagraph = new Paragraph("Reg.", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    regHeaderCell.AddElement(regHeaderParagraph);

                    var nomeHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var nomeHeaderParagraph = new Paragraph("Nome", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    nomeHeaderCell.AddElement(nomeHeaderParagraph);

                    var notaHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var notaHeaderParagraph = new Paragraph("Nota", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    notaHeaderCell.AddElement(notaHeaderParagraph);

                    var extensoHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var extensoHeaderParagraph = new Paragraph("Extenso", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    extensoHeaderCell.AddElement(extensoHeaderParagraph);

                    var rubricaHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var rubricaHeaderParagraph = new Paragraph("Rúbrica", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    rubricaHeaderCell.AddElement(rubricaHeaderParagraph);

                    var obsHeaderCell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE
                    };
                    var obsHeaderParagraph = new Paragraph("Observações", smallerFontBold)
                    {
                        Alignment = Element.ALIGN_CENTER
                    };
                    obsHeaderCell.AddElement(obsHeaderParagraph);

                    pautaTable.AddCell(numeroHeaderCell);
                    pautaTable.AddCell(regHeaderCell);
                    pautaTable.AddCell(nomeHeaderCell);
                    pautaTable.AddCell(notaHeaderCell);
                    pautaTable.AddCell(extensoHeaderCell);
                    pautaTable.AddCell(rubricaHeaderCell);
                    pautaTable.AddCell(obsHeaderCell);

                    pautaTable.HeaderRows = 1;

                    #endregion

                    #region Corpo da tabela

                    foreach (var linha in this.TabelaNotas.OrderBy(x => x.Nome))
                    {
                        var numeroCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var numeroParagraph = new Paragraph(linha.Numero, smallerFontBold)
                        {
                            Alignment = Element.ALIGN_RIGHT
                        };
                        numeroCell.AddElement(numeroParagraph);

                        var regCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var regParagraph = new Paragraph(linha.Reg, smallerFont)
                        {
                            Alignment = Element.ALIGN_CENTER
                        };
                        regCell.AddElement(regParagraph);

                        var nomeCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var nomeParagraph = new Paragraph(linha.Nome, smallerFont)
                        {
                            Alignment = Element.ALIGN_LEFT
                        };
                        nomeCell.AddElement(nomeParagraph);

                        var notaCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var notaParagraph = new Paragraph(linha.Nota, smallerFont)
                        {
                            Alignment = Element.ALIGN_RIGHT
                        };
                        notaCell.AddElement(notaParagraph);

                        var extensoCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var extensoParagraph = new Paragraph(linha.Extenso, smallerFont)
                        {
                            Alignment = Element.ALIGN_CENTER
                        };
                        extensoCell.AddElement(extensoParagraph);

                        var rubricaCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var rubricaParagraph = new Paragraph(linha.Rubrica, smallerFont)
                        {
                            Alignment = Element.ALIGN_CENTER
                        };
                        rubricaCell.AddElement(rubricaParagraph);

                        var obsCell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE
                        };
                        var obsParagraph = new Paragraph(linha.Observacoes, smallerFont)
                        {
                            Alignment = Element.ALIGN_LEFT
                        };
                        obsCell.AddElement(obsParagraph);

                        pautaTable.AddCell(numeroCell);
                        pautaTable.AddCell(regCell);
                        pautaTable.AddCell(nomeCell);
                        pautaTable.AddCell(notaCell);
                        pautaTable.AddCell(extensoCell);
                        pautaTable.AddCell(rubricaCell);
                        pautaTable.AddCell(obsCell);
                    }

                    doc.Add(pautaTable);

                    #endregion

                    #endregion

                    #region Fechar o documento

                    writer.CreateXmpMetadata();

                    doc.Close();

                    #endregion

                    byte[] bytes = stream.ToByteArray();

                    if (!simulating)
                    {
                        bytes = PDFTools.CreateSignatureField(bytes, newPageEvent.SignatureFieldRectangle, 1, "sig0");
                    }

                    sw.Stop();

                    var elapsed = sw.ElapsedMilliseconds;

                    return bytes;
                }
            }
            else
            {
                return null;
            }
        }

        private bool ReadDataFromIAcad()
        {
            try
            {
                string requestString = string.Format(AppConfigKeys.WebServices.IAcad.Endpoint + AppConfigKeys.WebServices.IAcad.Metodos.GetPauta, this.Codigo);

                string res = "";
                int statusCode = 500;

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestString);
                request.Method = "GET";

                request.Accept = "application/json;odata=verbose";

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    statusCode = (int)response.StatusCode;
                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    res = reader.ReadToEnd();
                }

                var pauta = new JavaScriptSerializer().Deserialize<PautaJSON>(res);

                this.NomeCurso = pauta.NomeCurso;
                this.ECTS = pauta.ECTS;

                return true;
            }
            catch (Exception ex)
            {
                string exData = string.Format("{0} - {1}", ex.GetType().ToString(), ex.Message);
                return false;
            }
        }

        #region Classes

        public class PautaJSON
        {
            public PautaJSON()
            {
                TabelaNotas = new List<LinhaPauta>();
            }

            public string Codigo;
            public string CodDisciplina;
            public string NomeDisciplina;
            public string CodCurso;
            public string NomeCurso;
            public string EpocaExame;
            public string TipoExame;
            public string ECTS;
            public string DataExame;
            public string AnoLetivo;
            public List<LinhaPauta> TabelaNotas;
        }
        public class LinhaPautaJSON
        {
            public string Numero;
            public string Reg;
            public string Nome;
            public string Nota;
            public string Extenso;
            public string Rubrica;
            public string Observacoes;
        }

        private static class Settings
        {
            public class PDF
            {
                private PDF() { }
                public static int SpaceBeforePauta
                {
                    get
                    {
                        return 20;
                    }
                }
                public static int TopMargin
                {
                    get
                    {
                        return 36;
                    }
                }
                public static string Morada
                {
                    get
                    {
                        return "Quinta de Prados 5000-801 Vila Real";
                    }
                }

                public static string DepartamentosLabel
                {
                    get
                    {
                        return "DEPARTAMENTOS E SERVIÇOS";
                    }
                }
                public static string Departamentos
                {
                    get
                    {
                        return "T  259 350 000 " + Encoding.GetEncoding(1252).GetString(new byte[] { (byte)149 }) + " F  259 350 480";
                    }
                }
                public static string ReitoriaLabel
                {
                    get
                    {
                        return "REITORIA";
                    }
                }
                public static string Reitoria
                {
                    get
                    {
                        return "T  259 350 166 " + Encoding.GetEncoding(1252).GetString(new byte[] { (byte)149 }) + " F  259 325 058";
                    }
                }
                public static string WWWLabel
                {
                    get
                    {
                        return "WWW";
                    }
                }
                public static string WWW
                {
                    get
                    {
                        return "www.utad.pt";
                    }
                }

                public static BaseFont CustomFont
                {
                    get
                    {
                        return BaseFont.CreateFont("c:\\Windows\\Fonts\\arial.ttf", "Windows-1252", BaseFont.EMBEDDED);
                    }
                }
                public static Int32 NoBorder
                {
                    get
                    {
                        return PdfPCell.NO_BORDER;
                    }
                }
                public static Int32 AllBorders
                {
                    get
                    {
                        return PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                    }
                }

                public static float DefaultParagraphLeading { get { return 10; } }
            }
        }

        public class LinhaPauta
        {
            public string Numero { get; set; }
            public string Reg { get; set; }
            public string Nome { get; set; }
            public string Nota { get; set; }
            public string Extenso { get; set; }
            public string Rubrica { get; set; }
            public string Observacoes { get; set; }

            public override string ToString()
            {
                return string.Format("{0} - {1}: {2} ({3}, {4})", Numero, Nome, Nota, Extenso, Observacoes);
            }
        }

        public class HeaderFooterBuilderPautas : PdfPageEventHelper
        {
            int pageCount;
            Pauta p = null;
            byte[] barcode;

            Int32 defBorder = Settings.PDF.NoBorder;

            Rectangle pageSize;

            public float HeaderHeightDifference { get; private set; }

            public Rectangle SignatureFieldRectangle { get; private set; }

            public HeaderFooterBuilderPautas(Pauta pauta, Rectangle _pageSize, int _pageCount = -1, float headerHeightDifference = 0)
            {
                p = pauta;
                pageCount = _pageCount;
                pageSize = _pageSize;
                barcode = ToBarcode(p.Codigo);
            }

            private static float CalculatePdfPTableHeight(PdfPTable table, int startingRow = 0, int endingRow = -1)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (Document doc = new Document(PageSize.A4))
                    {
                        using (PdfWriter w = PdfWriter.GetInstance(doc, ms))
                        {
                            doc.Open();

                            var top = doc.Top - doc.TopMargin;
                            var lel = table.WriteSelectedRows(startingRow, endingRow, doc.Left, doc.Top - doc.TopMargin, w.DirectContent);

                            doc.Close();
                            return top - lel;
                        }
                    }
                }
            }



            public override void OnEndPage(PdfWriter writer, Document document)
            {
                #region Settings

                var simulate = pageCount == -1;

                var titleFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var footerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                var footerBoldFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var footerLinesFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 4, iTextSharp.text.Font.BOLD, BaseColor.GRAY);

                var bodyFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                var bodyFontBold = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var bodyFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                var smallerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                var smallerFontBold = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var smallerFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                var smallFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);


                bool firstPage = writer.CurrentPageNumber == 1;

                #endregion

                #region Cabeçalho

                var topPos = GetHeader(writer.CurrentPageNumber).WriteSelectedRows(0, -1, document.Left, pageSize.Top - Settings.PDF.TopMargin, writer.DirectContent);

                var topMargin = CalculatePdfPTableHeight(GetHeader(writer.CurrentPageNumber + 1)) + Settings.PDF.TopMargin + Settings.PDF.SpaceBeforePauta;

                document.SetMargins(document.LeftMargin, document.RightMargin, topMargin, document.BottomMargin);
                

                #endregion

                #region Rodapé

                string paginaText = string.Format("{0}/{1}", writer.CurrentPageNumber.ToString(), pageCount);
                iTextSharp.text.Image barcodePaginas = iTextSharp.text.Image.GetInstance(ToBarcode(paginaText));

                var footer = new PdfPTable(2)
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    SpacingBefore = 0,
                    SpacingAfter = 0,
                    TotalWidth = document.Right - document.Left,
                };
                //footer.DefaultCell.Border = PdfPCell.NO_BORDER;
                footer.DefaultCell.Border = defBorder;
                footer.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                footer.SetWidths(new float[] { 11f, 89f });


                var footerLineHorizontal = new PdfPCell()
                {
                    Border = footer.DefaultCell.Border,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Colspan = 2
                };

                footerLineHorizontal.AddElement(new LineSeparator(footerLinesFont));

                var footerWhiteRow = new PdfPCell()
                {
                    Border = footer.DefaultCell.Border,
                    Colspan = 2
                };
                footerWhiteRow.FixedHeight = 3f;


                var footerPageInfo = new PdfPCell()
                {
                    //Border = PdfPCell.RIGHT_BORDER,
                    Border = defBorder,
                    HorizontalAlignment = PdfPCell.ALIGN_CENTER,
                    VerticalAlignment = PdfPCell.ALIGN_CENTER,
                    Rowspan = 2,
                    BorderWidthRight = 0.3f,
                    BorderColorRight = BaseColor.GRAY
                };


                footerPageInfo.Phrase = new Phrase(string.Format("Pauta|SIC|{1}{0}{0}pág: {2}", Environment.NewLine, DateTime.Now.Year, paginaText), footerFont);


                var footerContactosMorada = new PdfPTable(3)
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    SpacingBefore = 0,
                    SpacingAfter = 0,
                };
                footerContactosMorada.DefaultCell.Border = defBorder;
                footerContactosMorada.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                footerContactosMorada.WidthPercentage = 100;
                footerContactosMorada.TotalWidth = Convert.ToSingle(0.85f * (document.Right - document.Left));
                footerContactosMorada.SetWidths(new float[] { 50f, 35f, 15f });

                var footerMorada = new PdfPCell()
                {
                    Border = footer.DefaultCell.Border,
                    HorizontalAlignment = footerContactosMorada.DefaultCell.HorizontalAlignment,
                    VerticalAlignment = footerContactosMorada.DefaultCell.VerticalAlignment,
                    Colspan = 6
                };

                footerMorada.Phrase = new Phrase(Settings.PDF.Morada, footerFont);

                var footerContactosDep = new PdfPCell()
                {
                    Border = footerContactosMorada.DefaultCell.Border,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = footerContactosMorada.DefaultCell.VerticalAlignment
                };
                var footerContactosReitoria = new PdfPCell()
                {
                    Border = footerContactosMorada.DefaultCell.Border,
                    HorizontalAlignment = footerContactosMorada.DefaultCell.HorizontalAlignment,
                    VerticalAlignment = footerContactosMorada.DefaultCell.VerticalAlignment
                };
                var footerContactosWWW = new PdfPCell()
                {
                    Border = footerContactosMorada.DefaultCell.Border,
                    HorizontalAlignment = footerContactosMorada.DefaultCell.HorizontalAlignment,
                    VerticalAlignment = footerContactosMorada.DefaultCell.VerticalAlignment
                };

                var fcd = new Phrase();
                fcd.Add(new Chunk(Settings.PDF.DepartamentosLabel + "   ", footerBoldFont));
                fcd.Add(new Chunk(Settings.PDF.Departamentos, footerFont));
                var fcr = new Phrase();
                fcr.Add(new Chunk(Settings.PDF.ReitoriaLabel + "   ", footerBoldFont));
                fcr.Add(new Chunk(Settings.PDF.Reitoria, footerFont));
                var fwww = new Phrase();
                fwww.Add(new Chunk(Settings.PDF.WWWLabel + "   ", footerBoldFont));
                fwww.Add(new Chunk(Settings.PDF.WWW, footerFont));

                footerContactosDep.Phrase = fcd;
                footerContactosReitoria.Phrase = fcr;
                footerContactosWWW.Phrase = fwww;

                footerContactosMorada.AddCell(footerMorada);
                footerContactosMorada.AddCell(footerContactosDep);
                footerContactosMorada.AddCell(footerContactosReitoria);
                footerContactosMorada.AddCell(footerContactosWWW);

                var contactosMoradaHolderCell = new PdfPCell()
                {
                    Border = defBorder,
                    HorizontalAlignment = footer.DefaultCell.HorizontalAlignment,
                    VerticalAlignment = footer.DefaultCell.VerticalAlignment
                };

                contactosMoradaHolderCell.AddElement(footerContactosMorada);

                footer.AddCell(footerLineHorizontal);
                footer.AddCell(footerWhiteRow);
                footer.AddCell(footerPageInfo);
                footer.AddCell(contactosMoradaHolderCell);

                footer.WriteSelectedRows(0, -1, document.Left, document.Bottom, writer.DirectContent);

                #endregion

                base.OnStartPage(writer, document);
            }

            private PdfPTable GetHeader(int page)
            {
                #region Settings

                var simulate = pageCount == -1;

                var titleFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var footerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                var footerBoldFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var footerLinesFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 4, iTextSharp.text.Font.BOLD, BaseColor.GRAY);

                var bodyFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                var bodyFontBold = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var bodyFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 10, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                var smallerFont = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                var smallerFontBold = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                var smallerFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 8, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                var smallFontItalic = new iTextSharp.text.Font(Settings.PDF.CustomFont, 6, iTextSharp.text.Font.ITALIC, BaseColor.BLACK);

                #endregion

                iTextSharp.text.Image barcodeCodigo = iTextSharp.text.Image.GetInstance(barcode);
                barcodeCodigo.Alignment = iTextSharp.text.Image.ALIGN_LEFT | iTextSharp.text.Image.ALIGN_TOP | iTextSharp.text.Image.TEXTWRAP;

                bool firstPage = page == 1;

                using (Document document = new Document(PageSize.A4))
                {

                    var headerInfo = new PdfPTable(12)
                    {
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        SpacingBefore = 0,
                        SpacingAfter = Settings.PDF.SpaceBeforePauta,
                        TotalWidth = document.Right - document.Left
                    };
                    headerInfo.DefaultCell.Border = defBorder;

                    #region TOP

                    var saUTADCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 10
                    };
                    //var saUTADParagraph = new Paragraph("Serviços Académicos da UTAD", smallerFontItalic);
                    var saUTADParagraph = new Paragraph("Serviços Académicos da UTAD", smallerFontItalic);
                    saUTADParagraph.Alignment = Element.ALIGN_LEFT;
                    saUTADCell.AddElement(saUTADParagraph);

                    var aLetivoCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 2
                    };
                    var aLetivoParagraph = new Paragraph();
                    aLetivoParagraph.Add(new Chunk(string.Format("Ano Letivo\n"), smallerFontBold));
                    aLetivoParagraph.Add(new Chunk(string.Format(p.AnoLetivo), smallerFont));
                    aLetivoParagraph.Alignment = Element.ALIGN_CENTER;
                    aLetivoParagraph.Leading = Settings.PDF.DefaultParagraphLeading;
                    aLetivoCell.AddElement(aLetivoParagraph);

                    var titleCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 12
                    };
                    titleCell.AddElement(new Paragraph("PAUTA", titleFont) { Alignment = Element.ALIGN_CENTER, Leading = Settings.PDF.DefaultParagraphLeading });

                    #endregion

                    #region Código da pauta

                    var codCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 8
                    };

                    var codParagraph = new Paragraph();
                    codParagraph.Add(new Chunk("Código da Pauta: ", bodyFontBold));
                    codParagraph.Add(new Chunk(p.Codigo, bodyFont));
                    codCell.AddElement(codParagraph);


                    var codBarrasHeaderCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 6,
                        Rowspan = 2
                    };
                    codBarrasHeaderCell.AddElement(barcodeCodigo);

                    #endregion

                    #region Exame

                    var eExameCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = firstPage ? Element.ALIGN_LEFT : Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 2
                    };
                    eExameCell.AddElement(new Paragraph("Época de Exame:", bodyFontBold) { Alignment = eExameCell.HorizontalAlignment });
                    var eExameValueCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = firstPage ? Element.ALIGN_RIGHT : Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 2
                    };
                    eExameValueCell.AddElement(new Paragraph(p.EpocaExame, bodyFont) { Alignment = eExameValueCell.HorizontalAlignment });
                    var tExameCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = /*firstPage ? Element.ALIGN_LEFT : */Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 2
                    };
                    tExameCell.AddElement(new Paragraph("Tipo de Exame:", bodyFontBold) { Alignment = tExameCell.HorizontalAlignment });
                    var tExameValueCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = firstPage ? 1 : 2
                    };
                    tExameValueCell.AddElement(new Paragraph(p.TipoExame, bodyFont) { Alignment = tExameValueCell.HorizontalAlignment });
                    var ectsCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = /*firstPage ? Element.ALIGN_LEFT : */Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 2
                    };
                    ectsCell.AddElement(new Paragraph("ECTS:", bodyFontBold) { Alignment = ectsCell.HorizontalAlignment });
                    var ectsValueCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = firstPage ? 3 : 2
                    };
                    ectsValueCell.AddElement(new Paragraph(p.ECTS, bodyFont) { Alignment = ectsValueCell.HorizontalAlignment });

                    #endregion

                    #region UC

                    var cursoCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 9
                    };
                    var cursoParagraph = new Paragraph();
                    cursoParagraph.Add(new Chunk("Curso: ", bodyFontBold));
                    cursoParagraph.Add(new Chunk(p.Curso, bodyFont));
                    cursoParagraph.Alignment = Element.ALIGN_LEFT;
                    cursoCell.AddElement(cursoParagraph);

                    var dExameCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_BOTTOM,
                        Colspan = 3
                    };
                    var dExameParagraph = new Paragraph();
                    dExameParagraph.Add(new Chunk("Data Exame: ", bodyFontBold));
                    dExameParagraph.Add(new Chunk(p.DataExame, bodyFont));
                    dExameParagraph.Alignment = Element.ALIGN_RIGHT | Element.ALIGN_BOTTOM;
                    dExameCell.AddElement(dExameParagraph);

                    var disciplinaCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 8
                    };
                    var disciplinaParagraph = new Paragraph();
                    disciplinaParagraph.Add(new Chunk("Disciplina: ", bodyFontBold));
                    disciplinaParagraph.Add(new Chunk(p.Disciplina, bodyFont));
                    disciplinaParagraph.Alignment = Element.ALIGN_LEFT;
                    disciplinaCell.AddElement(disciplinaParagraph);

                    #endregion

                    #region BOT

                    var botCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 12
                    };
                    botCell.AddElement(new Paragraph("Ordenado por Curso, por Nome", smallFontItalic) { Alignment = Element.ALIGN_RIGHT });

                    #endregion

                    #region Manutenção

                    var emptyCell = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border
                    };
                    emptyCell.AddElement(new Paragraph("", bodyFont));

                    var emptyCell2 = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        Colspan = 2
                    };
                    emptyCell.AddElement(new Paragraph("", bodyFont));

                    var emptyCell4 = new PdfPCell()
                    {
                        Border = headerInfo.DefaultCell.Border,
                        Colspan = 4
                    };
                    emptyCell.AddElement(new Paragraph("", bodyFont));

                    #endregion

                    #region Cabeçalho
                    if (firstPage)
                    {

                        #region Construir tabela

                        headerInfo.AddCell(saUTADCell);
                        headerInfo.AddCell(aLetivoCell);

                        headerInfo.AddCell(titleCell);

                        headerInfo.AddCell(codCell);
                        headerInfo.AddCell(emptyCell4);

                        headerInfo.AddCell(codBarrasHeaderCell);
                        headerInfo.AddCell(emptyCell2);
                        headerInfo.AddCell(emptyCell4);
                        headerInfo.AddCell(emptyCell2);
                        headerInfo.AddCell(emptyCell4);

                        headerInfo.AddCell(disciplinaCell);
                        headerInfo.AddCell(emptyCell4);

                        headerInfo.AddCell(cursoCell);
                        headerInfo.AddCell(dExameCell);

                        headerInfo.AddCell(eExameCell);
                        headerInfo.AddCell(eExameValueCell);
                        headerInfo.AddCell(ectsCell);
                        headerInfo.AddCell(ectsValueCell);
                        headerInfo.AddCell(tExameCell);
                        headerInfo.AddCell(tExameValueCell);

                        headerInfo.AddCell(botCell);

                        #endregion

                        #region Campo assinatura

                        #region old

                        //var campoAssinaturaHeight = CalculatePdfPTableHeight(headerInfo, 2, 6);
                        //var campoAssinaturaWidth = headerInfo.Rows[4].GetCells().Last(x => x != null).Width;
                        //var campoAssinaturaURX = document.Right;
                        //var campoAssinaturaURY = document.Top + CalculatePdfPTableHeight(headerInfo, 2, -1) + Settings.PDF.SpaceBeforePauta;
                        //var campoAssinaturaLLX = campoAssinaturaURX - campoAssinaturaWidth;
                        //var campoAssinaturaLLY = campoAssinaturaURY - campoAssinaturaHeight;

                        #endregion

                        var campoAssinaturaHeight = CalculatePdfPTableHeight(headerInfo, 2, 6);
                        var campoAssinaturaWidth = headerInfo.Rows[4].GetCells().Last(x => x != null).Width;
                        var campoAssinaturaURX = document.Right;
                        var campoAssinaturaURY = pageSize.Top - CalculatePdfPTableHeight(headerInfo, 0, 2) - Settings.PDF.TopMargin;
                        var campoAssinaturaLLX = campoAssinaturaURX - campoAssinaturaWidth;
                        var campoAssinaturaLLY = campoAssinaturaURY - campoAssinaturaHeight;

                        SignatureFieldRectangle = new Rectangle(campoAssinaturaLLX, campoAssinaturaLLY, campoAssinaturaURX, campoAssinaturaURY);

                        #endregion
                    }
                    else
                    {
                        #region Construir tabela

                        headerInfo.AddCell(saUTADCell);
                        headerInfo.AddCell(aLetivoCell);

                        headerInfo.AddCell(titleCell);

                        headerInfo.AddCell(codCell);
                        headerInfo.AddCell(eExameCell);
                        headerInfo.AddCell(eExameValueCell);

                        headerInfo.AddCell(codBarrasHeaderCell);
                        headerInfo.AddCell(emptyCell2);
                        headerInfo.AddCell(tExameCell);
                        headerInfo.AddCell(tExameValueCell);
                        headerInfo.AddCell(emptyCell2);
                        headerInfo.AddCell(ectsCell);
                        headerInfo.AddCell(ectsValueCell);

                        headerInfo.AddCell(disciplinaCell);
                        headerInfo.AddCell(emptyCell4);

                        headerInfo.AddCell(cursoCell);
                        headerInfo.AddCell(dExameCell);

                        headerInfo.AddCell(botCell);

                        #endregion
                    }
                    #endregion

                    return headerInfo;
                }
            }

            public float GetFrontHeaderSize()
            {
                return CalculatePdfPTableHeight(GetHeader(1));
            }

            public byte[] ToBarcode(string s)
            {

                Barcode39 bc = new Barcode39();

                bc.Code = s;
                bc.BarHeight = 35;

                var bm = new System.Drawing.Bitmap(bc.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));

                MemoryStream ms = new MemoryStream();
                bm.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                return ms.ToArray();
            }
        }

        #endregion

    }

    #region Extensions

    /// <summary>
    /// Summary description for Extensions
    /// </summary>
    public static class Extensions
    {
        public static string NoFiletypeExtension(this string s)
        {
            try
            {
                return s.Substring(0, s.LastIndexOf('.'));
            }
            catch
            {
                return s;
            }
        }

        public static SecureString ToSecureString(this string s)
        {
            var res = new SecureString();

            foreach (var c in s)
            {
                res.AppendChar(c);
            }

            return res;
        }
        public static string GetContentType(string fileName)
        {
            var extension = Path.GetExtension(fileName);

            if (String.IsNullOrWhiteSpace(extension))
            {
                return null;
            }

            var registryKey = Registry.ClassesRoot.OpenSubKey(extension);

            if (registryKey == null)
            {
                return null;
            }

            var value = registryKey.GetValue("Content Type") as string;

            return String.IsNullOrWhiteSpace(value) ? null : value;
        }

        public static byte[] ToByteArray(this Stream s)
        {
            if (s is MemoryStream) return (s as MemoryStream).ToArray();

            byte[] res = new byte[s.Length];

            s.Read(res, 0, Convert.ToInt32(s.Length));
            s.Position = 0;

            return res;
        }

        public static Stream ToStream(this byte[] b)
        {
            return new MemoryStream(b);
        }



        public static string ToUTF8(this string original)
        {
            return original.Decode(Encoding.Default, Encoding.UTF8);
        }
        public static string FromUTF8(this string original)
        {
            return original.Decode(Encoding.UTF8, Encoding.Default);
        }
        public static string Decode(this string original, Encoding originalEnc, Encoding newEnc)
        {
            var originalBytes = originalEnc.GetBytes(original);
            return newEnc.GetString(originalBytes);
        }

        public static string ToJson(this IEnumerable<object> l)
        {
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(l);

            return json;
        }
    }

    #endregion

}
