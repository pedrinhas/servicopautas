﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoPautas.Errors
{
    public static class LogHelper
    {
        private static Severity MinimumSeverity = Severity.Info;

        public static void Log(Exception ex, Severity severity, string additionalInfo = "")
        {
            string message = string.IsNullOrWhiteSpace(additionalInfo) ? ex.Message : additionalInfo;
            int count = 0;
            do
            {
                message = string.Format("{0}{1} {4} - {2}: {3}", message, Environment.NewLine + (count == 0 ? "" : "   "), ex.GetType().ToString(), ex.Message, count);

                ex = ex.InnerException;
                count++;
            } while (ex != null);

            Log(message, severity);
        }
        public static void Log(string message, Severity severity)
        {
            if (severity >= LogHelper.MinimumSeverity)
            {
                string logMessage = string.Format("[{0}] {1}: {2}", DateTime.Now.ToString("hh:mm:ss"), severity.ToString().ToUpper(), message);

                System.Diagnostics.Debug.WriteLine(logMessage);
            }
        }
    }
}
