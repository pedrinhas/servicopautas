﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoPautas.Errors
{
    public enum Severity
    {
        Info,
        Warning,
        Error,
        Critical,
        Success
    }
}
