﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoPautas.Errors.Exceptions
{
    public class SIDEException : Exception
    {
        public SIDEException(string message) : base(message) { }
        public SIDEException() { }
    }
}
