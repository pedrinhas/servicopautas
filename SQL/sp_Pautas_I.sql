USE [pautas]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pautas_I]    Script Date: 03/03/2017 15:25:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_Pautas_I]
	@codPauta varchar(50)
	, @dataLacrado date = null
	, @horaLacrado time(0) = null
	, @idDocente nvarchar(50) = null
	, @estadoSIGAcad int = null
	, @importedGesDoc bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @existeID int

	select @existeID=id from t_Pauta where codPauta = @codPauta

	if @existeID is null
	begin
		INSERT INTO t_Pauta (codPauta, dataLacrado, horaLacrado, idDocente, estadoSIGAcad, importedGesDoc)
		values (@codPauta, @dataLacrado, @horaLacrado, @idDocente, @estadoSIGAcad, @importedGesDoc)
	end
	select max(id) from t_Pauta
END
