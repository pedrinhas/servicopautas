USE [pautas]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pautas_Estado_U]    Script Date: 03/03/2017 14:08:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Pautas_Estado_S]
	@codPauta nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT EstadoSIGAcad from t_Pauta where codPauta = @codPauta
END
