USE [pautas]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pautas_Estado_U]    Script Date: 03/03/2017 14:19:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_Pautas_Estado_U]
	@codPauta nvarchar(50)
    , @estadoSIGAcad int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE t_Pauta
	SET estadoSIGAcad = @estadoSIGAcad
	WHERE codPauta = @codPauta

	select 1
END
