-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- sp_Config_LastImport_U '2016-01-01 00:00:00'

alter PROCEDURE sp_Config_LastImport_U
	@date datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @dateToString nvarchar(19)

	set @dateToString = convert(nvarchar(19), @date, 120)

    -- Insert statements for procedure here
	UPDATE t_Config 
	SET Value=@dateToString
	where [key] = 'lastImport'

	select 1
END
GO
